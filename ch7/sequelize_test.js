const { sequelize } = require('./models');


const { User, Comment } = require('./models');
const { findAll } = require('./models/user');

// INSERT
// User.create({
//     name: 'yonguk',
//     age: 30,
//     married: false,
//     comment: '김용욱입니다',
// });

// Delete
// User.destroy({
//     where: {name: 'yonguk'},
// }).then((result) =>{
//     console.log(result);
// });

// SELECT
// User.findAll({})
//     .then((retult) => {
//         retult.forEach((user) =>{
//             console.log(user.dataValues);
//         });
//     })
//     .catch((err) => {
//         console.log(err);
//     })


const { Op } = require('sequelize');

const selectAll = async () => {
    try{
        const result = await User.findAll({});
        return result;
    } catch(err){
        throw new Error(err);
    }
}

const selectWithWhere = async () => {
    try{
        const result = await User.findAll({
            attributes:['name', 'age'],
            where:{
                married: 1,
                age:{ [Op.gt]: 30},
            },
        });
        return result;
    } catch (err){
        throw new Error(err);
    }
}


// SELECT id, name, FROM users WHERE married = 0 OR age > 30;
const selectWithWhereOr = async () => {
    try{
        const result = await User.findAll({
            attributes: ['id', 'name', 'age'],
            where: {
                [Op.or]: [{married: false}, {age: { [Op.gt]: 30 } }],
            }
        });
        return result;
    } catch(err){
        throw new Error(err);
    }
};

const selectOrderBy = async () => {
    try{
        const result = User.findAll({
            attributes:['id', 'name', 'age', 'comment'],
            order: [['age', 'DESC']],
            limit: 1,
            offset: 0,
        });
        return result;
    } catch(err){
        throw new Error(err );
    }
}

const update = async () => {
    try{
        const result = User.update({
            comment: '바꿀 내용',
        }, {
            where: {id: 2},
        });
    } catch(err){
        throw new Error(err);
    }
}

const deleteQuery = async() => {
    try{
        const result = User.destroy({
            where: {id:2},
        })
    } catch(err){
        throw new Error(err);
    }
}

const selectJoin = async ()=> {
    try{
        const result = await User.findOne({
            include:[{
                model:Comment,
            }],
            where:{
                id: 3
            }
        });
        return result;
    } catch(err){
        throw new Error(err);
    }
}

const selectJoinWhere = async () =>{
    const result = await User.findOne({
        include:[{
            model: Comment,
            where:{
                id: 1,
            },
            attributes: ['id']
        }],
    });
    return result;
}

const insertComment = async () => {
    try{
        const result = await Comment.create({
            commenter: 1,
            comment: 'zero comment 추가',
        });
    }catch(err){
        throw new Error(err);
    }
}


const exitQueries = async () => {
    let result;
    try{
        result = await sequelize.sync({ force:false });
        console.log('DB Connect Success');
        // SELECT name, married FROM nodejs.users;
        // result = await User.findAll({
        //     attributes:['name', 'married']
        // });
        // result.forEach((user) => {
        //     console.log(user.dataValues);
        // });
    
        // result = await selectWithWhere();
        // result = await selectWithWhereOr();
        // result = await selectOrderBy();
        // result = await update();
        // result = await deleteQuery();
        // console.log(result);
        // await insertComment();\
        // result = await selectJoin();
        // const comments = await result.getComments();
        // const comments = result.Comments;
        // console.log('Coments:',comments);

        // result = await selectJoinWhere();
        // const comments = await result.getComments({
        //     where:{
        //         id: 1,
        //     },
        //     attributes: ['id']
        // });
        // console.log(comments);

        // const user = await User.findOne({
        //     where:{id:1}
        // });
        // const comment = await Comment.create({
        //     commenter: 1,
        //     comment: 'zero comment 추가'
        // });
        // await user.addComment(comment.id);

        

    } catch (err){
        console.log(err);
    }
}

exitQueries();