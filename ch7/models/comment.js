const Sequelize = require('sequelize');

module.exports = class Comment extends Sequelize.Model{
    static init(sequelize){
        return super.init({
            comment:{
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            created_at:{
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: sequelize.NOW,
            },  
        },{
            sequelize,
            timestamps: false,
            modelName: 'Comment',
            tableName: 'comments',
            paranoid: true,
            charset: 'utf8',
            collate: 'utf8_general_ci',
        });
    }
    static associate(db){
        db.Comment.belongsTo(db.User, {foreignKey: 'commenter', targetKey:'id'});
    }
};