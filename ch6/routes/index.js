const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    // res.send('Hello, Express');
    res.render('index', {title: 'Node Express with nunjucks!'});
});


module.exports = router;