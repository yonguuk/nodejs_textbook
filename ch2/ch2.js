console.log('2.1.2 Template String');
const num1 = 1;
const num2 = 2;
const result = 3;
const str = `${num1} + ${num2} = ${result}`;
console.log(str);

console.log('2.1.3 Object Literal')
let es = 'ES';
let sayNode = function(){
    console.log('Node');
};

const newObject = {
    sayJS(){
        console.log('JS');
    },
    sayNode,
    [es+6]: 'Fantastic',
};

newObject.sayNode();
newObject.sayJS();
console.log(newObject.ES6);

console.log('2.1.4 Arrow Function');

function add1(x, y){
    return x+y;
}

const add2 = (x, y) => {
    return x+y;
}

const add3 = (x, y) => x+y;
const add4 = (x, y) => (x+y);

function not1(x){
    return !x;
}

const not2 = x => !x;

var relationship1 = {
    name: 'zero',
    friends: ['nero', 'hero', 'xero'],
    logFriends: function(){
        var that = this; // relationship1 을 가리키는 this를 that에 저장
        this.friends.forEach(function(friend){
            console.log(that.name, friend);
        });
    }
}

relationship1.logFriends();

const relationship2 = {
    name: 'zero',
    friends: ['nero', 'hero', 'xero'],
    logFriends(){
        this.friends.forEach(friend => {
            console.log(this.name, friend);
        });
    }
}

relationship2.logFriends();

var candyMachine = {
    status: {
        name: 'node',
        count: 5,
    },
    getCandy: function(){
        this.status.count--;
        return this.status.count;
    },
};

// var getCandy = candyMachine.getCandy;
// var count = candyMachine.status.count;

const candyMachine2 = {
    status: {
        name: 'node',
        count: 5,
    },
    getCandy(){
        this.status.count--;
        return this.status.count;
    },
};

const {getCandy, status: {count}} = candyMachine2;

// var array = ['nodejs', {}, 10, true];
// var node = array[0];
// var obj = array[1];
// var bool = array[3];

const array = ['nodejs', {}, 10, true];
const [node, obj, , bool] = array;


// var Human = function(type){
//     this.type = type || 'human';
// }
// Human.isHuman = function(human){
//     return human instanceof Human;
// }

// Human.prototype.breathe = function(){
//     alert('h-a-a-a-m');
// };

// var Zero = function(type, firstName, lastName){
//     Human.apply(this, arguments);
//     this.firstName = firstName;
//     this.lastName = lastName;
// };

// Zero.prototype = Object.create(Human.prototype);
// Zero.prototype.constructor = Zero; // 상속하는 부분
// Zero.prototype.sayName = function(){
//     alert(this.firstName + ' ' + this.lastName);
// }
// var oldZero = new Zero('human', 'Zero', 'Cho');
// Human.isHuman(oldZero);

class Human{
    constructor(type = 'human'){
        this.type = type;
    }

    static isHuman(human){
        return human instanceof Human;
    }

    breathe(){
        alert('h-a-a-a-m');
    }
}

class Zero extends Human{
    constructor(type, firstName, lastName){
        super(type);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    sayName(){
        super.breathe();
        alert(`${this.firstName} ${this.lastName}`);
    }
}

const newZero = new Zero('human', 'Zero', 'Cho');
Human.isHuman(newZero);

const condition = true; // true면 resolve, false면 reject
const promise = new Promise((resolve, reject) => {
    if(condition){
        resolve('성공');
    } else {
        reject('실패');
    }
});

// promise
//     .then((message) => {
//         console.log(message); // 성공(resolve) 한 경우 실행
//     })
//     .catch((error) => {
//         console.error(error); // 실패(reject)한 경우 실행
//     })
//     .finally(() => {
//         console.log('무조건'); // 끝나고 무조건 실행
//     });

// promise
//     .then((message) => {
//         return new Promise((resolve, reject) => {
//             resolve(message);
//         });
//     })
//     .then((message2) => {
//         console.log(message2);
//         return new Promise((resolve, reject) => {
//             resolve(message2);
//         });
//     })
//     .then((message3) => {
//         console.log(message3);
//     })
//     .catch((error) => {
//         console.error(error);
//     });

// function findAndSaveUser(Users){
//     Users.findOne({}, (err, user) => { // 첫 번째 콜백
//         if(err){
//             return console.error(err);
//         }

//         user.name = 'zero';
//         user.save((err) => {
//             if(err){
//                 return console.error(err);
//             }
//             Users.findOne({ gender: 'm'}, (err, user) => {
//                 // 생략
//             });
//         });
//     });
// }

// function findAndSaveUser(Users){
//     Users.findOne({})
//         .then((user) => {
//             user.name = 'zero';
//             return user.save();
//         })
//         .then((user) => {
//             return URLSearchParams.findOne({ gender: 'm' });
//         })
//         .then((user) => {
//             // 생략
//         })
//         .catch(err => {
//             console.error(err);
//         });
// }

const promise1 = Promise.resolve('성공1');
const promise2 = Promise.resolve('성공2');
Promise.all([promise1, promise2])
    .then((result) => {
        console.log(result);
    })
    .catch((error) => {
        console.error(error);
    });

async function findAndSaveUser(Users){
    try{
        let user = await Users.findOne({});
        user.name = 'zero';
        user = await user.save();
        user = await Users.findOne({ gender: 'm'});
    } catch(error){
        console.error(error);
    }
}

const findAndSaveUser = async (Users) => {
    try{
        let user = await Users.findOne({});
        user.name = 'zero';
        user = await user.save();
        user = await Users.findOne({ gender: 'm'});
    } catch(error){
        console.error(error);
    }
}

const promise1 = Promise.resolve('성공1');
const promise2 = Promise.resolve('성공2');
(async () =>{
    for await (promise of [promise1, promise2]){
        console.log(promise);
    }
})();