// const http = require('http');
// const server = http.createServer((req, res) => {
//     res.writeHead(200, { 'Content-Type': 'text/html; charset-utf-8' });
//     res.write('<h1>Hello Node!</h1>');
//     res.end('<p>Hello Server!</p>');
// })

// server.listen(8080, () => {
//     console.log('8080번 포트에서 서버 대기중');
// });

// server.on('error', (error) =>{
//     console.error(error);
// });

// const server2 = http.createServer((req, res) => {
//     res.writeHead(200, { 'Content-Type': 'text/html; charset-utf-8' });
//     res.write('<h1>Hello Node!</h1>');
//     res.end('<p>Hello Server2!</p>');
// });

// server2.listen(8081, () => {
//     console.log('8081번 포트에서 대기중');
// });
const http = require('http');
const fs = require('fs').promises;

const server = http.createServer(async (req, res) => {
    try{
        const data = await fs.readFile('./home.html');
        res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        res.end(data);
    } catch(err){
        console.error(err);
        writeHead(500, {'Content-Type': 'text/plain; charset=utf-8'});
        res.end(err.message);
    }
});

server.listen(8080, () => {
    console.log('8080포트 서버 대기중');
});