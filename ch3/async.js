const fs = require('fs').promises;

fs.readFile('./readme.txt')
    .then((data) => {
        console.log('첫 번째 파일 읽기!');
    })
    .catch((err) =>{
        console.error(err);
    });

fs.readFile('./readme.txt')
    .then((data) => {
        console.log('두 번째 파일 읽기!');
    })
    .catch((err) =>{
        console.error(err);
    });

fs.readFile('./readme.txt')
    .then((data) => {
        console.log('세 번째 파일 읽기!');
    })
    .catch((err) => {
        console.error(err);
    });