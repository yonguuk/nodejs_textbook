// const fs = require('fs');

// console.log('시작');
// fs.readFile('./readme.txt', (err, data) => {
//     if(err)
//         throw err;
//     console.log('1번', data.toString());

//     fs.readFile('./readme.txt', (err, data) => {
//         if(err)
//             throw err;
//         console.log('2번', data.toString());
        
//         fs.readFile('./readme.txt', (err, data) => {
//             if(err)
//                 throw err;
//             console.log('3번', data.toString());
//         });
//     });
// });

const fs = require('fs').promises;

fs.readFile('./readme.txt')
    .then((data) => {
        console.log('1번');
        return fs.readFile('./readme.txt');
    })
    .then((data) => {
        console.log('2번');
        return fs.readFile('./readme.txt');
    })
    .then((data) => {
        console.log('3번');
        return fs.readFile('./readme.txt');
    })
    .catch((err) => {
        console.error(err);
    })