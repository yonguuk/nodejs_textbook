const url = require('url');
const { URL } = url;

// WHATWG 방식
const myURL = new URL('http://www.gilbut.co.kr/book/bookList.aspx?sercate1=00100100#anchor');
console.log('new URL():', myURL);
console.log('url.format():', url.format(myURL));
console.log('------------------------------');
const parsedUrl = url.parse('http://www.gilbut.co.kr/book/bookList.aspx?sercate1=00100100#anchor');
console.log('url.parse():', parsedUrl);
console.log('url.format():', url.format(parsedUrl));