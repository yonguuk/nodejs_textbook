console.log('require가 가장 먼저 오지 않아도 됩니다');

module.exports = '나를 찾아줘';

require('./var');

console.log('require.cache');
console.log(require.cache);
console.log('require.main');
console.log(require.main === module);
console.log(require.main.filename);